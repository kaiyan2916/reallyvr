﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionObjectBase : MonoBehaviour
{
    public EatAgent EatAgent;
    public bool isactivate;
    public bool ishold;
    public bool isfirst;
    protected ObjectAudioManager Audio;
    //Material material;
    //Material[] materials;
    // Start is called before the first frame update
    public virtual void Start()
    {
        isactivate = false;
        ishold = false;
        isfirst = false;
        Audio = GameObject.Find("ObjectAudioManager").GetComponent<ObjectAudioManager>();
        //try
        //{
        //    material = this.GetComponent<Material>();
        //}
        //catch (System.Exception)
        //{
        //    Debug.Log("no material");
        //    throw;
        //}
        //materials = this.GetComponentsInChildren<Material>();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        //if (isfirst == true)
        //{
        //    material.color.
        //    for (int i = 0; i < materials.Length; i++)
        //    {
        //        materials[i].color = Color.black;
        //    }
        //}
    }
    public virtual void Effect()
    {

    }
    public virtual void SelectEnter()
    {
        this.GetComponent<Collider>().isTrigger = true;
        this.GetComponent<Rigidbody>().useGravity = false;
    }
    public virtual void SelectOver()
    {
        this.GetComponent<Collider>().isTrigger = false;
        this.GetComponent<Rigidbody>().useGravity = true;
    }
    public float CheckAngle(float value)
    {
        float angle = value - 180;
        if (angle > 0)
        {
            return angle - 180;
        }
        return angle + 180;
    }
    public float Xposition(float orginx)
    {
        float inputminx = 0;
        float inputmaxx = 0;
        float minx = 0;
        float maxx = 0;
        if (EatAgent.gameObject.name == "Prick")
        {
            inputminx = 1.6f;
            inputmaxx = 2.25f;
            minx = 14f;
            maxx = 18.57f;

            if (orginx < inputminx)
            {
                orginx = inputminx;
            }
            else if (orginx > inputmaxx)
            {
                orginx = inputmaxx;
            }
            return (maxx - minx) / (inputmaxx - inputminx) * (orginx - inputminx) + minx;
        }
        else if (EatAgent.gameObject.name == "Man")
        {
            inputminx = 0.1f;
            inputmaxx = 0.74f;
            minx = 5.09f;
            maxx = 9.7f;
            if (orginx < inputminx)
            {
                orginx = inputminx;
            }
            else if (orginx > inputmaxx)
            {
                orginx = inputmaxx;
            }
            return (maxx - minx) / (inputmaxx - inputminx) * (orginx - inputminx) + minx;
        }
        return 0;
    }
    public float YPosition(float orginy)
    {
        float inputminy = 2.4f;
        float inputmaxy = 2.55f;
        float miny = -88.7f;
        float maxy = -88f;
        if (orginy < inputminy)
        {
            orginy = inputminy;
        }
        else if (orginy > inputmaxy)
        {
            orginy = inputmaxy;
        }
        return (maxy - miny) / (inputmaxy - inputminy) * (orginy - inputminy) + miny;
    }
}
