﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pepper_can : InteractionObjectBase
{
    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
        //if (ishold)
        //{
        //    if (CheckAngle(this.transform.eulerAngles.x) > 5)
        //    {
        //        isactivate = true;
        //    }
        //}
        if (CheckAngle(this.transform.eulerAngles.x) > 5)
        {
            isactivate = true;
        }
        else
        {
            isactivate = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PrickFood")
        {
            EatAgent = GameObject.Find("EatArea/Prick").GetComponent<EatAgent>();
        }
        else if (other.tag == "ManFood")
        {
            EatAgent = GameObject.Find("EatArea/Man").GetComponent<EatAgent>();
        }
        if (EatAgent != null)
        {
            if (isfirst == false && isactivate == true)
            {
                Effect();
                isfirst = true;
            }
        }
    }
    public override void Effect()
    {
        if (PhotonNetwork.IsMasterClient == true)
        {
            EatAgent.SendServer(Food.pepper_can.GetHashCode(), EatAgent.Thirstynumber, EatAgent.peenumber, EatAgent.waternumber, EatAgent.foodnumber);
        }

        //EatAgent.ispepper = true;
        isactivate = false;
    }
    public override void SelectEnter()
    {
        base.SelectEnter();
        //ishold = true;
    }
    public override void SelectOver()
    {
        base.SelectOver();
        //isactivate = false;
        //ishold = false;
    }
}
