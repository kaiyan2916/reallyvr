﻿using Game.Net;
using Game.View;
using ProtoMsg;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
namespace Game.View
{
    public class LoginWindow : BaseWindow
    {
        public LoginWindow()
        {
            resName = "LoginWindow";
            resident = false;
            selfType = WindowType.LoginWindow;
            scenesType = ScenesType.Login;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);
        }
        InputField AccountInput;
        InputField PwdInput;
        protected override void Awake()
        {
            base.Awake();
            AccountInput = transform.Find("AccountInput").GetComponent<InputField>();
            PwdInput = transform.Find("pwdInput").GetComponent<InputField>();
        }

        protected override void OnAddListener()
        {
            base.OnAddListener();
            NetEvent.Instance.AddEventListener(1000, handleUserRegisterS2C);
            NetEvent.Instance.AddEventListener(1001, handleUserLoginS2C);
        }
        /// <summary>
        /// 返回登入結果
        /// </summary>
        /// <param name="obj"></param>
        private void handleUserLoginS2C(BufferEntity response)
        {
            UserLoginS2C s2cMSG = ProtobufHelper.FromBytes<UserLoginS2C>(response.proto);
            switch (s2cMSG.Result)
            {
                case 0:
                    Debug.Log("登入成功!");
                    SceneManager.LoadScene("Game");
                    Close();
                    break;
                case 1:
                    Debug.Log("存在敏感字詞");
                    WindowManager.Instance.ShowTips("存在敏感字詞!");//打開提示窗體
                    break;
                case 2:
                    Debug.Log("帳號密碼不匹配");
                    WindowManager.Instance.ShowTips("帳號密碼不匹配!"); //打開提示窗體
                    break;
                default:
                    break;
            }
        }

        private void handleUserRegisterS2C(BufferEntity response)
        {
            UserRegisterS2C s2cMSG = ProtobufHelper.FromBytes<UserRegisterS2C>(response.proto);
            switch (s2cMSG.Result)
            {
                case 0:
                    Debug.Log("註冊成功!");
                    WindowManager.Instance.ShowTips("註冊成功!");//打開提示窗體 提示
                    break;
                case 1:
                    Debug.Log("存在敏感字詞");
                    WindowManager.Instance.ShowTips("存在敏感字詞!");//打開提示窗體 提示
                    break;
                case 2:
                    Debug.Log("長度不夠");
                    WindowManager.Instance.ShowTips("長度不夠!");//打開提示窗體 提示
                    break;
                case 3:
                    Debug.Log("帳號已被註冊");
                    WindowManager.Instance.ShowTips("帳號已被註冊!");//打開提示窗體 提示
                    break;
                default:
                    break;
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnRemoveListener()
        {
            base.OnRemoveListener();
        }

        protected override void RegisterUIEvent()
        {
            base.RegisterUIEvent();
            for (int i = 0; i < buttonList.Length; i++)
            {
                switch (buttonList[i].name)
                {
                    case "Register_button":
                        buttonList[i].onClick.AddListener(RegisterBtnOnClick);
                        break;
                    case "login_button":
                        buttonList[i].onClick.AddListener(LoginBtnOnClick);
                        break;
                    default:
                        break;
                }
            }

        }


        private void LoginBtnOnClick()
        {
            if (string.IsNullOrEmpty(AccountInput.text))
            {
                Debug.Log("帳號不可為空");
                WindowManager.Instance.ShowTips("帳號不可為空!");
                return;
            }
            if (string.IsNullOrEmpty(PwdInput.text))
            {
                Debug.Log("密碼不可為空");
                WindowManager.Instance.ShowTips("密碼不可為空!");
                return;
            }
            UserLoginC2S c2sMSG = new UserLoginC2S();
            c2sMSG.UserInfo = new UserInfo();
            c2sMSG.UserInfo.Account = AccountInput.text;
            c2sMSG.UserInfo.Password = PwdInput.text;

            BufferFactory.CreateAndSendPackage(1001, c2sMSG);
        }

        private void RegisterBtnOnClick()
        {
            if (string.IsNullOrEmpty(AccountInput.text))
            {
                Debug.Log("帳號不可為空");
                WindowManager.Instance.ShowTips("帳號不可為空!");
                return;
            }
            if (string.IsNullOrEmpty(PwdInput.text))
            {
                Debug.Log("密碼不可為空");
                WindowManager.Instance.ShowTips("密碼不可為空!");
                return;
            }
            UserRegisterC2S c2sMSG = new UserRegisterC2S();
            c2sMSG.UserInfo = new UserInfo();
            c2sMSG.UserInfo.Account = AccountInput.text;
            c2sMSG.UserInfo.Password = PwdInput.text;

            BufferFactory.CreateAndSendPackage(1000, c2sMSG);
        }
    }
}

