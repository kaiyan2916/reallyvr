﻿using Game.Net;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Liquid
{
    water,
    cola,
    wine,
    vitmain,
    laxative,
    lemon,
}
public class WaterBottle : MonoBehaviour
{
    public EatAgent eatAgent;
    public Sprite[] water;
    public Sprite[] cola;
    public Sprite[] wine;
    public Sprite[] vitmain;
    public Sprite[] laxative;
    public Sprite[] lemon;
    public SpriteRenderer bottle;
    public GameObject add,add_bottle;
    public bool isactive;
    public bool iscola;
    public bool iswine;
    public bool isvitamin;
    public bool islaxative;
    public bool islemon;
    public int waterbottlenumber;
    public bool iscolafirst;
    public bool iswinefirst;
    public bool isvitaminfirst;
    public bool islaxativefirst;
    public bool islemonfirst;
    public int state;
    // Start is called before the first frame update
    void Start()
    {
        waterbottlenumber = 0;
        bottle = this.GetComponent<SpriteRenderer>();
        isactive = false;
        iscola = false;
        iswine = false;
        isvitamin = false;
        islaxative = false;
        islemon = false;
        iscolafirst = false;
        iswinefirst = false;
        isvitaminfirst = false;
        islaxativefirst = false;
        islemonfirst = false;
        state = Liquid.water.GetHashCode();
    }
    public void Resetbottle()
    {
        iscola = false;
        iswine = false;
        isvitamin = false;
        islaxative = false;
        islemon = false;
        isactive = false;
        state = Liquid.water.GetHashCode();
    }
    // Update is called once per frame
    void Update()
    {
        //BattleListener.Instance.PlayerLiquid(HandleLiquidCMD);
        if (eatAgent.waternumber <= 0)
        {
            Resetbottle();
        }
        //if (isactive == true)
        //{
        //    return;
        //}
        //if (iscola == true)
        //{
        //    if (iscolafirst == false)
        //    {
        //        eatAgent.isaction = true;
        //        eatAgent.waternumber = 100;
        //        add_bottle = PhotonNetwork.Instantiate(add.name, add.transform.position, add.transform.rotation);
        //        add_bottle.SetActive(true);
        //        add_bottle.GetComponent<Animator>().SetBool("cola", true);
        //        Invoke("Destroyadd", 0.8f);
        //        StartCoroutine(FillCola());
        //        isactive = true;
        //        iscolafirst = true;
        //    }
        //}
        //else if (iswine == true)
        //{
        //    if (iswinefirst == false)
        //    {
        //        eatAgent.isaction = true;
        //        eatAgent.waternumber = 100;
        //        add_bottle = PhotonNetwork.Instantiate(add.name, add.transform.position, add.transform.rotation);
        //        add_bottle.SetActive(true);
        //        add_bottle.GetComponent<Animator>().SetBool("wine", true);
        //        Invoke("Destroyadd", 0.8f);
        //        StartCoroutine(FillWine());
        //        isactive = true;
        //        iswinefirst = true;
        //    }
        //}
        //else if (isvitamin == true)
        //{
        //    if (isvitaminfirst == false)
        //    {
        //        eatAgent.isaction = true;
        //        bottle.sprite = vitmain[waterbottlenumber];
        //        isactive = true;
        //        isvitaminfirst = true;
        //    }
        //}
        //else if (islaxative == true)
        //{
        //    if (islaxativefirst == false)
        //    {
        //        eatAgent.isaction = true;
        //        bottle.sprite = laxative[waterbottlenumber];
        //        isactive = true;
        //        islaxativefirst = true;
        //    }
        //}
        //else if (islemon == true)
        //{
        //    if (islemonfirst == false)
        //    {
        //        eatAgent.isaction = true;
        //        bottle.sprite = lemon[waterbottlenumber];
        //        isactive = true;
        //        islemonfirst = true;
        //    }
        //}
    }
    public void Drink()
    {
        waterbottlenumber = 0;
        if (eatAgent.waternumber >= 100)
        {
            waterbottlenumber = 6;
        }
        if (eatAgent.waternumber > 80)
        {
            waterbottlenumber = 5;
        }
        else if (eatAgent.waternumber > 60)
        {
            waterbottlenumber = 4;
        }
        else if (eatAgent.waternumber > 40)
        {
            waterbottlenumber = 3;
        }
        else if (eatAgent.waternumber > 20)
        {
            waterbottlenumber = 2;
        }
        else if (eatAgent.waternumber > 10)
        {
            waterbottlenumber = 1;
        }
        else
        {
            waterbottlenumber = 0;
        }
        if (iscola == true)
        {
            bottle.sprite = cola[waterbottlenumber];
        }
        else if (iswine == true)
        {
            bottle.sprite = wine[waterbottlenumber];
        }
        else if (isvitamin == true)
        {
            bottle.sprite = vitmain[waterbottlenumber];
        }
        else if (islaxative == true)
        {
            bottle.sprite = laxative[waterbottlenumber];
        }
        else if (islemon == true)
        {
            bottle.sprite = lemon[waterbottlenumber];
        }
        else
        {
            bottle.sprite = water[waterbottlenumber];
        }
    }
    public void FillWater()
    {
        bottle.sprite = water[6];
        Resetbottle();
    }
    public IEnumerator FillCola()
    {
        for (int i = waterbottlenumber; i < 7; i++)
        {
            bottle.sprite = cola[i];
            yield return new WaitForSeconds(0.3f);
        }
    }
    public IEnumerator FillWine()
    {
        for (int i = waterbottlenumber; i < 7; i++)
        {
            bottle.sprite = wine[i];
            yield return new WaitForSeconds(0.3f);
        }
    }
    public void SendServer(int charid,int liquid, int thirstynumber, int peenumber, int waternumber, float foodnumber)
    {
        Debug.Log("發送伺服器");
        eatAgent.isaction = true;
        byte[] charids = BitConverter.GetBytes(charid);
        byte[] thirsty = BitConverter.GetBytes(thirstynumber);
        byte[] pee = BitConverter.GetBytes(peenumber);
        byte[] water = BitConverter.GetBytes(waternumber);
        byte[] foodnumbers = BitConverter.GetBytes(foodnumber);
        byte[] liquids = BitConverter.GetBytes(liquid);
        byte[] data = new byte[24];
        Array.Copy(charids, 0, data, 0, 4);
        Array.Copy(thirsty, 0, data, 4, 4);
        Array.Copy(pee, 0, data, 8, 4);
        Array.Copy(water, 0, data, 12, 4);
        Array.Copy(foodnumbers, 0, data, 16, 4);
        Array.Copy(liquids, 0, data, 20, 4);
        BufferFactory.CreateAndSendPackage(3, data);
    }
    public void Handle(int liquid)
    {
        switch (liquid)
        {
            case 0:
                eatAgent.waterBottle.isactive = false;
                break;
            case 1:
                eatAgent.waterBottle.iscola = true;
                if (eatAgent.waterBottle.iscolafirst == false)
                {
                    eatAgent.isaction = true;
                    eatAgent.waternumber = 100;
                    eatAgent.waterBottle.add_bottle = PhotonNetwork.Instantiate(eatAgent.waterBottle.add.name, eatAgent.waterBottle.add.transform.position, eatAgent.waterBottle.add.transform.rotation);
                    eatAgent.waterBottle.add_bottle.SetActive(true);
                    eatAgent.waterBottle.add_bottle.GetComponent<Animator>().SetBool("cola", true);
                    Invoke("Destroyadd", 0.8f);
                    StartCoroutine(eatAgent.waterBottle.FillCola());
                    eatAgent.waterBottle.isactive = true;
                    eatAgent.waterBottle.iscolafirst = true;
                }
                break;
            case 2:
                eatAgent.waterBottle.iswine = true;
                if (eatAgent.waterBottle.iswinefirst == false)
                {
                    eatAgent.isaction = true;
                    eatAgent.waternumber = 100;
                    eatAgent.waterBottle.add_bottle = PhotonNetwork.Instantiate(eatAgent.waterBottle.add.name, eatAgent.waterBottle.add.transform.position, eatAgent.waterBottle.add.transform.rotation);
                    eatAgent.waterBottle.add_bottle.SetActive(true);
                    eatAgent.waterBottle.add_bottle.GetComponent<Animator>().SetBool("wine", true);
                    Invoke("Destroyadd", 0.8f);
                    StartCoroutine(eatAgent.waterBottle.FillWine());
                    eatAgent.waterBottle.isactive = true;
                    eatAgent.waterBottle.iswinefirst = true;
                }
                break;
            case 3:
                eatAgent.waterBottle.isvitamin = true;
                if (eatAgent.waterBottle.isvitaminfirst == false)
                {
                    eatAgent.isaction = true;
                    eatAgent.waterBottle.bottle.sprite = eatAgent.waterBottle.vitmain[eatAgent.waterBottle.waterbottlenumber];
                    eatAgent.waterBottle.isactive = true;
                    eatAgent.waterBottle.isvitaminfirst = true;
                }
                break;
            case 4:
                eatAgent.waterBottle.islaxative = true;
                if (eatAgent.waterBottle.islaxativefirst == false)
                {
                    eatAgent.isaction = true;
                    eatAgent.waterBottle.bottle.sprite = eatAgent.waterBottle.laxative[eatAgent.waterBottle.waterbottlenumber];
                    eatAgent.waterBottle.isactive = true;
                    eatAgent.waterBottle.islaxativefirst = true;
                }
                break;
            case 5:
                eatAgent.waterBottle.islemon = true;
                if (eatAgent.waterBottle.islemonfirst == false)
                {
                    eatAgent.isaction = true;
                    eatAgent.waterBottle.bottle.sprite = eatAgent.waterBottle.lemon[eatAgent.waterBottle.waterbottlenumber];
                    eatAgent.waterBottle.isactive = true;
                    eatAgent.waterBottle.islemonfirst = true;
                }
                break;
            default:
                break;
        }
    }
    void Destroyadd()
    {
        if (eatAgent.waterBottle.add_bottle != null)
        {
            GameObject.Destroy(eatAgent.waterBottle.add_bottle);
        }
        eatAgent.isaction = false;
    }
}
