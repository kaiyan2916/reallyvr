﻿using Game.Net;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleHandle : MonoBehaviour
{
    public EatAgent prick, man;
    public EatAgent main;
    private void Update()
    {
        BattleListener.Instance.PlayerAction(HandleActionCMD);
        BattleListener.Instance.PlayerLiquid(HandleLiquidCMD);
    }
    void HandleActionCMD(byte[] s2cMSG)
    {
        int charid = BitConverter.ToInt32(s2cMSG, 0);
        if (charid == 1)
        {
            main = prick;
        }
        else if (charid == 2)
        {
            main = man;
        }

        if (main != null)
        {
            int thiry = BitConverter.ToInt32(s2cMSG, 4);
            int pee = BitConverter.ToInt32(s2cMSG, 8);
            int water = BitConverter.ToInt32(s2cMSG, 12);
            float foodnumber = BitConverter.ToSingle(s2cMSG, 16);
            int food = BitConverter.ToInt32(s2cMSG, 20);
            main.Thirstynumber = thiry;
            main.peenumber = pee;
            main.waternumber = water;
            main.foodnumber = foodnumber;
            switch (food)
            {
                case 0:
                    main.issweat = true;
                    break;
                case 1:
                    main.ispepper = true;
                    break;
                case 2:
                    main.iseatrock = true;
                    break;
                case 3:
                    main.issock = true;
                    break;
                default:
                    break;
            }
            Debug.Log("處理飯訊息");
        }
    }
    public void HandleLiquidCMD(byte[] s2cMSG)
    {
        int charid = BitConverter.ToInt32(s2cMSG, 0);
        if (charid == 1)
        {
            main = prick;
        }
        else if (charid == 2)
        {
            main = man;
        }
        if (main != null)
        {
            int thiry = BitConverter.ToInt32(s2cMSG, 4);
            int pee = BitConverter.ToInt32(s2cMSG, 8);
            int water = BitConverter.ToInt32(s2cMSG, 12);
            float foodnumber = BitConverter.ToSingle(s2cMSG, 16);
            int liquid = BitConverter.ToInt32(s2cMSG, 20);
            main.Thirstynumber = thiry;
            main.peenumber = pee;
            main.waternumber = water;
            main.foodnumber = foodnumber;
            main.waterBottle.state = liquid;
            main.waterBottle.Handle(liquid);
            Debug.Log("處理水杯訊息");
        }
    }

}
